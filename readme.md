What happens here ? 
* number n grows from 0 to infinity (See :   for (n=1; n<1000000 ) 
*  rotation number t grows from 0.38 to 0.5
* on parameter plane : point c moves along boundary of main cardioid toward c=0.75 ( root point of period 2 component of Mandelbrot set)
*  on dynamic plane there is a sequence of Siegel discs which ends at parabolic flower ( two sepals)
** Points 
*** 2 points of period=2 cycle moves toward :
****critical orbit 
**** fixed point alpha 
**** themselves 
***For n= infinity these 5 points coincidies  ( gluing ). It is parabolic fixed point with external rays 1/3 and 2/3 
*** fixed point alpha moves toward  z=-1/2
** Curves : simple closed curve ( non-self intersecting) is glued. Then there are 2 curves sharing one common points.





==to do==
* add final image : c= -3/4 ( root point ) with 4 mainn chessboard boxes
* add period 2 cycle with external ray that land on it ( 1/3 and 2/3)
* add fixed point

-----------------------------------------------------------------------------------------------

I have made it with significant help of Claude Heiland-Allen and Wolf Jung. This image is based on the idea taken from image by Arnaud Chéritat[1].

  program in c language
  for console
  it uses qd library ( libqd) for quad double precision 

  --------- description --------------
  https://commons.wikimedia.org/wiki/File:InfoldingSiegelDisk1over3.gif
  https://en.wikibooks.org/wiki/Fractals/Iterations_in_the_complex_plane/siegel#1.2F3

  program draws series of images 
  of critical orbit 
  for complex quadratic polynomial
  fc(z) = z^2 + c
  where parameter c is computed from integer n 
  near t=1/2 

  c = c(n) = c(t(n))
  -----------------------------
  to change t change : 
  - GiveT
  - some code in BackwardOrbit which chooses good preimage. 
  It is specyfic to t. Here see lines : 443-445
  





  ---------- git ------------------

  cd existing_folder
  git init
  git remote add origin git@gitlab.com:adammajewski/InfoldingSiegelDisk_in_c_1over2_quaddouble.git
  git add c.c
  git commit -m " first commit"
  git push -u origin master







```C
/*

old procedure : 


  compute floating point number from continued fraction
  t(n) = [0; 2, n, phi]

  = 0 +  1/ (2 + (1 /(n + (1/phi))))
  

  input n > 0 !!!
  output t

*/



void GiveT(int n,  double t[4]){

  double p[4], a[4], b[4], one[4],  two[4];

  double phi[4];

  
  //
  c_qd_copy_d(1.0, one); // one = 1.0
  c_qd_copy_d(2.0, two); // two = 2.0
  c_qd_copy_d(10.0, a); // a = 10.0
  // a
  //c_qd_npwr(a, n, p); // p = a^n
  c_qd_copy_d((double) n, p); // p = n 

  GivePhi(phi); 
  c_qd_div(one,phi,b); // b= one/phi = 1/phi
  
  c_qd_add(p,b,t); // t= p+b = p+1/phi = (a^n) +1/phi
 
  c_qd_copy(one,b);
  c_qd_selfdiv(t,b); //b= 1/t
 
  c_qd_add(two, b, t); // t = 2+b   
 

  c_qd_copy(one,b);
  c_qd_selfdiv(t,one); //t= 1/b
 

  c_qd_copy(one,t); 
 

}

