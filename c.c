/*

  program in c language
  for console
  it uses qd library ( libqd) for quad double precision 

  --------- description --------------
  https://commons.wikimedia.org/wiki/File:InfoldingSiegelDisk1over3.gif
  https://en.wikibooks.org/wiki/Fractals/Iterations_in_the_complex_plane/siegel#1.2F3

  program draws series of images 
  of critical orbit 
  for complex quadratic polynomial
  fc(z) = z^2 + c
  where parameter c is computed from integer n 
  near t=1/2 

  c = c(n) = c(t(n))
  -----------------------------
  to change t change : 
  - GiveT
  - some code in BackwardOrbit which chooses good preimage. 
  It is specyfic to t. Here see lines : 443-445
  
  
  ------------------------------------------
  
  What happens here ? 
* number n grows from 0 to infinity (See :   for (n=1; n<1000000 ) 
*  rotation number t grows from 0.38 to 0.5
* on parameter plane : point c moves along boundary of main cardioid toward c=0.75 ( root point of period 2 component of Mandelbrot set)
*  on dynamic plane there is a sequence of Siegel discs which ends at parabolic flower ( two sepals)
** Points 
*** 2 points of period=2 cycle moves toward :
****critical orbit 
**** fixed point alpha 
**** themselves 
***For n= infinity these 5 points coincidies  ( gluing ). It is parabolic fixed point with external rays 1/3 and 2/3 
*** fixed point alpha moves toward  z=-1/2
** Curves : simple closed curve ( non-self intersecting) is glued. Then there are 2 curves sharing one common points.

--------------------------------------------------------------------------------------------
  
  
  
  
  to do:
  
add final image : c= -3/4 ( root point ) with 4 mainn chessboard boxes
add period 2 cycle with external ray that land on it ( 1/3 and 2/3)
add fixed point



  ------- compile and run ---------------
  gcc c.c -lm -lqd -Wall -DUSE_QD_REAL -march=native
  ./a.out 





  ---------- git ------------------

  cd existing_folder
  git init
  git remote add origin git@gitlab.com:adammajewski/InfoldingSiegelDisk_in_c_1over2_quaddouble.git
  git add c.c
  git commit -m " first commit"
  git push -u origin master

  


*/

#include <stdio.h>
#include <stdlib.h> // malloc
#include <string.h> // strcat
#include <math.h> //sqrt
#include <limits.h>
#include <qd/c_qd.h> // qd library :  quad double number with precision = 64 decimal digits 




// virtual 2D array = rectangle = matrix 
unsigned int iWidth = 1000;
unsigned int  iHeight = 1000;





// memmory 1D array 
unsigned char *data;
unsigned int  iLength; // = langth ( data)




/* colors */
const unsigned int MaxColorComponentValue=255; /* color component is coded from 0 to 255 ;  it is 8 bit color file */
const int iExterior = 0; /* exterior of Julia set */
const int iBoundary = 255; /* border , boundary, Julia set */




/* world coordinate */
double ZxMin = -1.0;
double ZxMax =  1.0;
double ZyMin = -1.0;
double ZyMax =  1.0;


 
double PixelWidth ;
double PixelHeight ;
double invPixelWidth ;
double invPixelHeight ;




// n -> t -> c =cx+cy*i
int n;
double t[4];
double cx[4];
double cy[4];

// fixed point alfa = z = a = ax+ay*i
double ax;
double ay;
// period 2 cycle : {z1,z2}
double z1x;
double z1y;
double z2x;
double z2y;

// number of iterations 
unsigned long long int iMaxForward;
unsigned long long int iMaxBackward;



















// ------------- functions -------------------------------------

/* phi = (1+sqrt(5))/2 = golden ratio
   https://en.wikipedia.org/wiki/Golden_ratio

   input = none
   output = p 

*/
void GivePhi(double p[4]){
 

  double a[4];
  
  


  c_qd_copy_d(5.0, p); // p = 5.0
  c_qd_copy_d(1.0, a); // a = 1.0

  c_qd_sqrt(p, p); // p = sqrt(p) =sqrt(5)
  c_qd_add(p, a, p); // p= a+p = 1+sqrt(5)
  c_qd_selfmul_d(0.5, p); // p = p*0.5 = p/2 = phi
  
  
}



/*


  compute floating point number from continued fraction
  t(n) = [0; 2, n, phi]

  = 0 +  1/ (2 + (1 /(n + (1/phi))))
  

  input n > 0 !!!
  output t

*/



void GiveT(int n,  double t[4]){

  double p[4], a[4], b[4], one[4],  two[4];

  double phi[4];

  
  //
  c_qd_copy_d(1.0, one); // one = 1.0
  c_qd_copy_d(2.0, two); // two = 2.0
  c_qd_copy_d(10.0, a); // a = 10.0
  // a
  //c_qd_npwr(a, n, p); // p = a^n
  c_qd_copy_d((double) n, p); // p = n 

  GivePhi(phi); 
  c_qd_div(one,phi,b); // b= one/phi = 1/phi
  
  c_qd_add(p,b,t); // t= p+b = p+1/phi = (a^n) +1/phi
 
  c_qd_copy(one,b);
  c_qd_selfdiv(t,b); //b= 1/t
 
  c_qd_add(two, b, t); // t = 2+b   
 

  c_qd_copy(one,b);
  c_qd_selfdiv(t,one); //t= 1/b
 

  c_qd_copy(one,t); 
 

}



/* 
   compute complex number c = point on the boundary of period 1 component of Mandelbrot set 
  
   double a = t *2*M_PI; // from turns to radians
   double cx, cy; 
   // c = cx+cy*i 
   cx = (cos(a))/2-(cos(2a))/4; 
   cy = (sin(a))/2-(sin(2a))/4;

   input t
   output cx,cy  where c = cx + cy*I

*/
void GiveC( double t[4], double cx[4], double cy[4]){
  double  a[4];
  double a2[4];
  double  p[4];
  // 
  double   s[4];
  double  s2[4];



  c_qd_pi(p);
  
  c_qd_selfmul_d(2.0,p); // p = 2 *pi
  c_qd_mul(t, p,  a);

  c_qd_mul_qd_d(a, 2.0, a2); // a2 = 2*t*p
  //
  c_qd_cos(a,s);   // s  = cos(a)
  c_qd_cos(a2,s2); // s2 = cos(a2)
  //
  c_qd_selfdiv_d(2.0,s);   // s  = s/2
  c_qd_selfdiv_d(4.0,s2);  // s2 = s/4 
  //
  c_qd_sub(s,s2,cx); // cx = s - s2 

  //

  c_qd_sin(a,s);   // s  = sin(a)
  c_qd_sin(a2,s2); // s2 = sin(a2)
  //
  c_qd_selfdiv_d(2.0,s);   // s  = s/2
  c_qd_selfdiv_d(4.0,s2);  // s2 = s/4 
  //
  c_qd_sub(s,s2,cy); // cy = s - s2 




}



// The call by reference method of passing arguments to a function
void GiveAlfaFixedPoint(const double c_x, const double c_y, double* ax, double* ay)
{
  // Equation defining fixed points : z^2-z+c = 0
  // a = 1, b = -1 c = c
  double dx, dy; //The discriminant is the  d=b^2- 4ac = dx+dy*i
  double r; // r(d)=sqrt(dx^2 + dy^2)
  // http://math.stackexchange.com/questions/44406/how-do-i-get-the-square-root-of-a-complex-number
  // \sqrt{z}=\sqrt{r}\frac{z+r}{|z+r|},\quad r=|z|
  double sx, sy; // s = sqrt(d) = sqrt((r+dx)/2)+-sqrt((r-dx)/2)*i = sx + sy*i
  // alfa = ax+ay*I
 
 // d=1-4c = dx+dy*i
 dx = 1 - 4*c_x;
 dy = -4 * c_y;
 // r(d)=sqrt(dx^2 + dy^2)
 r = sqrt(dx*dx + dy*dy);
 //sqrt(d) = s =sx +sy*i
 sx = sqrt((r+dx)/2);
 sy = sqrt((r-dx)/2);
 // alfa = ax +ay*i = (1-sqrt(d))/2 = (1-sx + sy*i)/2
 *ax = 0.5 - sx/2.0;
 *ay =  sy/2.0;
 
// alfa =  ax+ay*I;
}

// The call by reference method of passing arguments to a function
// z1 =f(z2) and f(f(z1)) = z1 and f(f(z2))=z2
void GivePeriod2Points(const double c_x, const double c_y, double* z1x, double* z1y, double* z2x, double* z2y)
{
  // (reduced) equation defining perid 2 points :  z^2+z+c+1=0
  // a = 1, b = 1 c = c+1
  double dx, dy; //The discriminant is the  d=b^2- 4ac = dx+dy*i
  double r; // r(d)=sqrt(dx^2 + dy^2)
  double sx, sy; // s = sqrt(d) 
  
 
 // d= b^2 - 4ac = 1-4(c+1) = 1-4c-4 = -4c-3 = -4cx-3 + ( -4cy)*i
 dx = -4*c_x - 3;
 dy = -4*c_y;

 // r(d)=sqrt(dx^2 + dy^2)
 r = sqrt(dx*dx + dy*dy);
 //sqrt(d) = s =sx +sy*i
 sx = sqrt((r+dx)/2);
 sy = sqrt((r-dx)/2);
 // z1 = z1x +z1y*i = (1-sqrt(d))/2 = (1-sx + sy*i)/2
 *z1x = -0.5 - sx/2.0;
 *z1y =  sy/2.0;
 *z2x = -0.5 + sx/2.0;
 *z2y = -sy/2.0;
 

}


int DrawDoublePoint( double Zx, double Zy, unsigned char A[])
{
  unsigned int iX,iY; /* indices of 2D virtual array (image) = integer coordinate */
  unsigned int i; /* index of 1D array  */ 
 

   if (Zx < ZxMin || ZxMax  < Zx || Zy < ZyMin || ZyMax < Zy) {  printf("   point z = %f , %f out of bounds  \n", Zx, Zy); return -1; } 

  
  iX = (int)((Zx-ZxMin)*invPixelWidth);
  iY = (int)((ZyMax-Zy)*invPixelHeight); // reverse Y axis
  i = iX + iY*iWidth;//f(iX,iY);
  
  
  A[i] = iBoundary;  /* draw */
  return 0;
}





int DrawPoint( double Zx[4], double Zy[4], unsigned char A[])
{
  unsigned int iX,iY; /* indices of 2D virtual array (image) = integer coordinate */
  unsigned int i; /* index of 1D array  */ 
 

   //if (Zx[0] < ZxMin || ZxMax  < Zx[0] || Zy[0] < ZyMin || ZyMax < Zy[0]) {  printf("   point z = %f , %f out of bounds  \n", Zx[0], Zy[0]); return -1; } 

  
  iX = (int)((Zx[0]-ZxMin)*invPixelWidth);
  iY = (int)((ZyMax-Zy[0])*invPixelHeight); // reverse Y axis
  i = iX + iY*iWidth;//f(iX,iY);
  
  
  A[i] = iBoundary;  /* draw */
  return 0;
}






/*

  forward iteration without explicit use of complex number 
  f(z) = z^2 + c = complex quadratic polynomial
  -----------------
  tmp = 2 * zx * zy + cy;
  zx= zx2 - zy2 + cx;
  zy = tmp;

  input: 
  z0 = z0x + z0y*i
  iMaxF


*/

int ForwardOrbit(const double z0x[4], const double z0y[4], unsigned long long int iMaxF){


  
  unsigned long long int i; // iteration number
  double tmp[4];
  
  double zx[4];
  double zy[4];
  double zx2[4];
  double zy2[4];


  c_qd_copy(z0x, zx);
  c_qd_copy(z0y, zy); 

 
  for (i=0; i<iMaxF; ++i) {
    
    // manual debug
    //printf("i = %llu \n", i);
    //printf("zx = "); c_qd_write(zx); 
    //printf("zy = "); c_qd_write(zy); 

    // tmp = 2 * zx * zy + cy;
    c_qd_mul_qd_d(zx, 2.0, tmp); // tmp = 2*zx
    c_qd_selfmul(zy, tmp);   // temp  = temp*zy = 2*zx*zy
    c_qd_selfadd(cy, tmp);   // temp  = temp+cy = 2*zx*zy +cy
    // zx= zx2 - zy2 + cx;
    c_qd_sqr(zx, zx2); // zx2 = zx*zx
    c_qd_sqr(zy, zy2); // zy2 = zy*zy
    c_qd_sub(zx2,zy2, zx); // zx = zx2-zy2
    c_qd_selfadd(cx, zx);   // zx  <- zx + cx = zx2-zy2 +cx
    // zy = tmp;
    c_qd_copy(tmp, zy); 
    // no escape ( bailout ) test !!
    
    DrawPoint(zx, zy, data);



    

  }

  return 0;
}




/*

  backward iteration without explicit use of complex number 
  f^(-1)(z) = sqrt(z- c) 
  ------- subtraction ------
  b  = z  - c = bx + by*i
  bx = zx - cx
  by = zy - cy 
  ---------- principal value of complex square -----
  For every non-zero complex number z 
  there exist precisely two numbers w such that 
  w2 = b
  
  - the principal square root of z (defined below) :  w= sqrt(b)
  - its negative : -w = sqrt(b)

  w = sqrt(b)  = wx + wy*i
  r = abs(b) = sqrt(bx*bx + by*by)
  wx = sqrt((r+bx)/2)
  wy = sqrt((r-bx)/2)

  the sign of the wy is :
  - the same as the sign of by, 
  - positive when zero
  -------------  choose good preimage ----------------------
  
  ---------------- 
  z = f^(-1)(z ) = sqrt(z-c)
  z=w

*/

int BackwardOrbit(const double z0x[4], const double z0y[4], unsigned long long int iMaxB){
   
   

  double  zx[4];
  double  zy[4];

  // b 
  double  bx[4];
  double  by[4];
  double  bx2[4]; // bx2 = bx*bx
  double  by2[4];
  double  r[4]; // r = abs(b)


  double  tmp[4];

  c_qd_copy(z0x, zx);
  c_qd_copy(z0y, zy); 


  unsigned long long int i; // iteration number
    
  for (i=0; i<iMaxB; ++i) {

    //--- manual debug ----------
    //printf("i = %llu \n", i );
    //printf("zx = "); c_qd_write(zx); 
    //printf("zy = "); c_qd_write(zy);  

    // -------- b = z - c ---------
    c_qd_sub(zx, cx, bx); // bx = zx - cx
    c_qd_sub(zy, cy, by); // by = zy - cy

      
    // -------- z = sqrt(b) --------------
    c_qd_sqr(bx, bx2); // bx2 = bx*bx
    c_qd_sqr(by, by2); // by2 = by*by
    c_qd_add(bx2, by2, r);   // r = bx2 + by2 
    c_qd_sqrt(r,r); // r = sqrt(r)
    
    // zx
    c_qd_add(r,bx, tmp);   // tmp = r + bx
    c_qd_selfdiv_d(2.0, tmp); // tmp = tmp/2.0
    c_qd_sqrt(tmp,zx); // zx = sqrt(tmp)
     
    // zy
    c_qd_sub(r,bx, tmp);   // tmp = r - bx
    c_qd_selfdiv_d(2.0, tmp); // tmp = tmp/2.0
    c_qd_sqrt(tmp,zy); // zy = sqrt(tmp)

     
    // the sign of the wy is  the same as the sign of by, 
    if (by[0]<0.0 ) 
      c_qd_neg(zy,zy); //zy = -zy 

    //-------------  choose good preimage here A -----------
    // https://en.wikibooks.org/wiki/Fractals/mandel#complex_quadratic_polynomial
    // aproximation of rays landing on the critical point by line y = f(x) = x  
    if (zy[0]/zx[0]<1.0){
      c_qd_neg(zx,zx); //zx = -zx
      c_qd_neg(zy,zy); //zy = -zy 
    }
    //

    DrawPoint(zx, zy, data); 
     
  }  
   

  return 0;
}


int CriticalOrbit(unsigned long long int iMax_F, unsigned long long int iMax_B){

  
  double  z0x[4];
  double  z0y[4];
  
  // critical point z = zx+zy*i = 0
  c_qd_copy_d(0.0, z0x); // zx= = 0.0
  c_qd_copy_d(0.0, z0y); // zy = 0.0

  ForwardOrbit( z0x,z0y, iMax_F); // forward iteration of critical point 
  BackwardOrbit(z0x,z0y, iMax_B); // backward iteration of critical point 
 




  return 0;
}


unsigned long long int Give_iMaxForward( int n){
   
  if (n <       100) return     1000000;
  if (n <      1000) return    10000000;
  if (n <     10000) return    10000000;
  if (n <    100000) return   100000000;
  if (n <   1000000) return  1000000000;
  if (n <  10000000) return 10000000000;
  
  return      10000000000000;
 
}

unsigned long long int Give_iMaxBackward( int n){
   
  if (n <       100) return     100;
  if (n <      1000) return    1000;
  if (n <     10000) return    1000;
  if (n <    100000) return    1000;
  if (n <   1000000) return    1000;
  if (n <  10000000) return    1000;
  
  return     1000;
 
}



// prints out important informations
void info(int n_){


  printf("\n\n ------------------------------------------ \n\n");
  printf("n = %d \n", n_ );
  printf("t = "); c_qd_write(t); 
  printf("cx = "); c_qd_write(cx); 
  printf("cy = "); c_qd_write(cy);  
  printf("alfa fixed point z = ax = %.16f ; %.16f \n", ax, ay);
  printf("period 2 cycle = {z1, z2} = {(%.16f ; %.16f), (%.16f ; %.16f)} \n", z1x, z1y, z2x, z2y);
  printf("iMaxForward = %llu\n", iMaxForward );
  printf("iMaxBackward = %llu\n", iMaxBackward );
  printf("\n\n");

}


int ClearArray( unsigned char A[] )
{
  int i; /* index of 1D array  */
  for(i=0;i<iLength;++i)  A[i]=iExterior;
  return 0;
}


// save data array to pgm file 
int SaveArray2PGMFile( unsigned char A[], double k, char* comment )
{
  
  FILE * fp;
  const unsigned int MaxColorComponentValue=255; /* color component is coded from 0 to 255 ;  it is 8 bit color file */
  char name [100]; /* name of file */
  snprintf(name, sizeof name, "%.0f", k); /*  */
  char *filename =strncat(name,".pgm", 4);
  
  
  
  /* save image to the pgm file  */      
  fp= fopen(filename,"wb"); /*create new file,give it a name and open it in binary mode  */
  fprintf(fp,"P5\n # %s\n %u %u\n %u\n", comment, iWidth, iHeight, MaxColorComponentValue);  /*write header to the file*/
  fwrite(A,iLength,1,fp);  /*write image data bytes to the file in one step */
  
  //
  printf("File %s saved. \n", filename);
  //if (comment == NULL)  printf ("empty comment \n");
  //                 else printf (" comment = %s \n", comment); 
  fclose(fp);

  return 0;
}



void MakeImage(int _n){


  ClearArray(data);


  if (_n==INT_MAX) { // then t reaches it's limit t=1/2 
  
        c_qd_copy_d( 0.5 ,  t  ); // t  =  0.5;
        c_qd_copy_d(-0.75,  cx ); // cx = -0.75;
        c_qd_copy_d( 0.0,   cy ); // cy =  0.0;
        
        //          draw line  from critical to fixed and it's inverse
        // draw inverse of critical
        // draw line critical to first inverse of critical
        // draw inverse of this line     
        
        
      }
  
    else {

      // n -> t
      GiveT(_n,t); // continued fraction
  
      // t -> c
      GiveC(t, cx,cy);
      
      // c -> critical orbit
      iMaxForward   = Give_iMaxForward(_n);
      iMaxBackward  = Give_iMaxBackward(_n);
      GiveAlfaFixedPoint(cx[0], cy[0], &ax, &ay);
      GivePeriod2Points(cx[0], cy[0], &z1x, &z1y,  &z2x, &z2y);
      info(_n);
      CriticalOrbit(iMaxForward, iMaxBackward);
  
      DrawDoublePoint( ax, ay, data);
      DrawDoublePoint( z1x, z1y, data);
      DrawDoublePoint( z2x, z2y, data);
      
  } 
 
   // draw externla rays 1/3 and 2/3 landing on period 2 orbit 
 

  SaveArray2PGMFile(data, _n, "test");

 


}



int Step(int n){

  if (n <    10) return     1;
  if (n <    20) return     2;
  if (n <    30) return     4;
  if (n <    50) return     5;
  if (n <    70) return     7;
  if (n <   100) return    10;
  if (n <   150) return    15;
  if (n <   300) return    30;
  if (n <   500) return    50;
  if (n <   700) return   100;
  if (n <  1000) return   300;
  if (n <  3000) return   500;
  if (n <  6000) return   800;
  if (n <  8000) return  1000;
  if (n < 10000) return  3000;
  if (n < 30000) return  6000;
  if (n < 60000) return  9000;
  if (n < 90000) return 12000;
  return 30000;
}




// init = all procedures before start of main computations 
int setup(void){


  iLength = iWidth*iHeight; // size = number of points in array 
  
  //
  PixelWidth  =  ((ZxMax-ZxMin)/iWidth);
  PixelHeight =  ((ZyMax-ZyMin)/iHeight);
  invPixelWidth  = 1 / PixelWidth;
  invPixelHeight = 1 / PixelHeight;




 
  /* create dynamic 1D arrays for colors ( shades of gray ) */
  
  data = malloc( iLength * sizeof(unsigned char) );
  
  if (data == NULL )
    {
      fprintf(stderr," Could not allocate memory\n");
      return 1;
    }
  

  fpu_fix_start(NULL); // libqd : turns on the round-to-double bit in the FPU control word on Intel x86 Processors. 
  return 0;
}





// all procedures before end of the program
void terminate(void){

  //
  fpu_fix_end(NULL); // libqd : 
  
  free(data);

}




// ------------------------------------------------------------------------------------------------------------------------

int main(void) {

 


  setup(); 
 

  // n from 1 not 0 
//  for (n=100000; n<10000000; n=n+ Step(n))  
   
    //    MakeImage(n); // for t tending to 1/2
 
 
  MakeImage(INT_MAX); //  t = 1/2 => c= -3/4
 
  terminate();

  return 0; 
}
